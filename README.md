# General Improvement VS Code Extension Pack

Collection of extensions of general improvements languages' agnostic

## Extensions Included

### File Utils

[![Latest Release][file-utils-version-bagde]][file-utils-url]
[![Downloads][file-utils-download-badge]][file-utils-url]
[![Rating][file-utils-rating-bagde]][file-utils-url]

A convenient way of creating, duplicating, moving, renaming, deleting files and directories
directly from your sidebar and the command palette


### Select Line Status Bar

[![Latest Release][selectline-statusbar-version-bagde]][selectline-statusbar-url]
[![Downloads][selectline-statusbar-download-badge]][selectline-statusbar-url]
[![Rating][selectline-statusbar-rating-bagde]][selectline-statusbar-url]

Displays selected lines count in status bar


### Sort Lines

[![Latest Release][sort-lines-version-bagde]][sort-lines-url]
[![Downloads][sort-lines-download-badge]][sort-lines-url]
[![Rating][sort-lines-rating-bagde]][sort-lines-url]

Sort lines of text. The following types of sorting are supported:

* `Sort lines` - Regular character code based sort (`F9`)**\***
* `Sort lines (case insensitive)` - Case insensitive sort
* `Sort lines (line length)` - Sort lines by line length
* `Sort lines (reverse)` - Reverse character code based sort
* `Sort lines (unique)` - Regular character code keeping only unique items
* `Sort lines (shuffle)` - Shuffles the lines randomly
* `Sort lines (natural)` - [Natural sort order][natural-order-url], sorts alphabetically but groups multi-digit numbers

**\*** _Note that this overrides the toggle breakpoint keybinding, you can unbind it by adding this to your `keybindings.json` file:_

`{ "key": "f9", "command": "-sortLines.sortLines", "when": "editorTextFocus" }`


### Contextual Duplicate

[![Latest Release][contextual-duplicate-version-bagde]][contextual-duplicate-url]
[![Downloads][contextual-duplicate-download-badge]][contextual-duplicate-url]
[![Rating][contextual-duplicate-rating-bagde]][contextual-duplicate-url]

This extension duplicates text based on the context

If text is selected, only the selected text will be duplicated

Otherwise, the complete line will be duplicated


### Partial Diff

[![Latest Release][partial-diff-version-bagde]][partial-diff-url]
[![Downloads][partial-diff-download-badge]][partial-diff-url]
[![Rating][partial-diff-rating-bagde]][partial-diff-url]

Compare (diff) text selections within a file, across different files, or to the clipboard


### Presentation Mode

[![Latest Release][presentation-mode-version-bagde]][presentation-mode-url]
[![Downloads][presentation-mode-download-badge]][presentation-mode-url]
[![Rating][presentation-mode-rating-bagde]][presentation-mode-url]

Show your code properly in a presentation



## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look


[file-utils-url]:            https://marketplace.visualstudio.com/items?itemName=sleistner.vscode-fileutils
[file-utils-version-bagde]:  https://img.shields.io/vscode-marketplace/v/sleistner.vscode-fileutils.svg
[file-utils-download-badge]: https://img.shields.io/vscode-marketplace/d/sleistner.vscode-fileutils.svg
[file-utils-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/sleistner.vscode-fileutils.svg

[selectline-statusbar-url]:            https://marketplace.visualstudio.com/items?itemName=tomoki1207.selectline-statusbar
[selectline-statusbar-version-bagde]:  https://img.shields.io/vscode-marketplace/v/tomoki1207.selectline-statusbar.svg
[selectline-statusbar-download-badge]: https://img.shields.io/vscode-marketplace/d/tomoki1207.selectline-statusbar.svg
[selectline-statusbar-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/tomoki1207.selectline-statusbar.svg

[sort-lines-url]:            https://marketplace.visualstudio.com/items?itemName=Tyriar.sort-lines
[sort-lines-version-bagde]:  https://img.shields.io/vscode-marketplace/v/Tyriar.sort-lines.svg
[sort-lines-download-badge]: https://img.shields.io/vscode-marketplace/d/Tyriar.sort-lines.svg
[sort-lines-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/Tyriar.sort-lines.svg
[natural-order-url]:         https://www.wikiwand.com/en/Natural_sort_order

[contextual-duplicate-url]:            https://marketplace.visualstudio.com/items?itemName=lafe.contextualduplicate
[contextual-duplicate-version-bagde]:  https://img.shields.io/vscode-marketplace/v/lafe.contextualduplicate.svg
[contextual-duplicate-download-badge]: https://img.shields.io/vscode-marketplace/d/lafe.contextualduplicate.svg
[contextual-duplicate-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/lafe.contextualduplicate.svg

[partial-diff-url]:            https://marketplace.visualstudio.com/items?itemName=ryu1kn.partial-diff
[partial-diff-version-bagde]:  https://img.shields.io/vscode-marketplace/v/ryu1kn.partial-diff.svg
[partial-diff-download-badge]: https://img.shields.io/vscode-marketplace/d/ryu1kn.partial-diff.svg
[partial-diff-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/ryu1kn.partial-diff.svg

[presentation-mode-url]:            https://marketplace.visualstudio.com/items?itemName=jspolancor.presentationmode
[presentation-mode-version-bagde]:  https://img.shields.io/vscode-marketplace/v/jspolancor.presentationmode.svg
[presentation-mode-download-badge]: https://img.shields.io/vscode-marketplace/d/jspolancor.presentationmode.svg
[presentation-mode-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/jspolancor.presentationmode.svg

[merge-request-url]: https://gitlab.com/pinage404/general-improvement-vs-code-extension-pack/merge_requests
[issue-url]:         https://gitlab.com/pinage404/general-improvement-vs-code-extension-pack/issues
