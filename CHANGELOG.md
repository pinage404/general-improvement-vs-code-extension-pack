## v0.2.1 / 2018-04-05

* Upd: simpler icon

## v0.2.0 / 2018-04-04

* Add: icon
* Fix: repository URL in README

## v0.1.0 / 2018-04-01

* First release
